# PXE pour le capitole du libre

## tftp server
sudo dnsmasq -z --interface=br0 --dhcp-range=192.168.20.1,192.168.20.254,255.255.255.0 --dhcp-broadcast -d -C dnsmasq.conf

## nfs server
change /etc/nfs.conf rootdir par le chemin

echo "/tftpboot *(ro,no_subtree_check)" > /etc/exports

exportfs -ra

systemctl start nfs-server

### test nfs
showmount -e

## qemu test
ip link add br0 type bridge
ip a a 192.168.20.1/24 dev br0
ip link set dev br0 up
echo "allow br0" > /etc/qemu/bridge.conf

qemu-system-x86_64 -smp 2 -m 1G -net nic -net bridge,br=br0 -boot n --vga virtio -enable-kvm
