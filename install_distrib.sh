#!/bin/sh
MENU=tftpboot/pxelinux.cfg/additional_menu_entries

URL=$1
TITLE=$2
DOWN_DIR=$3
INST_DIR=$4
ARCH=$5

wget -q $URL
tar xzf netboot.tar.gz
rm netboot.tar.gz

sed -i "s|$DOWN_DIR|$INST_DIR|g" $(grep -rl -e "$DOWN_DIR" $DOWN_DIR/)

echo -e "menu begin $TITLE\n\tmenu title $TITLE\n" >> $MENU
cat $DOWN_DIR/$ARCH/pxelinux.cfg/default | sed 's/^/  /' >> $MENU
echo -e "\tmenu separator\n\tlabel mainmenu\n\t\tmenu label ^Back\n\t\tmenu exit\nmenu end" >> $MENU

mv $DOWN_DIR tftpboot/$INST_DIR
