FROM ferrarimarco/pxe

RUN rm /var/lib/tftpboot/pxelinux.cfg/additional_menu_entries

COPY install_distrib.sh .
# Download and extract Debian Buster amd64
RUN ./install_distrib.sh http://ftp.nl.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/netboot.tar.gz "Debian Buster Installer" debian-installer debian-buster_amd64 amd64


# Download and extract Ubuntu 18.04
RUN ./install_distrib.sh http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-amd64/current/images/netboot/netboot.tar.gz "Ubuntu 18.04 Installer" ubuntu-installer ubuntu-18_amd64 amd64

# Download and extract Ubuntu 19.10
RUN ./install_distrib.sh http://archive.ubuntu.com/ubuntu/dists/eoan/main/installer-amd64/current/images/netboot/netboot.tar.gz "Ubuntu 19.10 Installer" ubuntu-installer ubuntu-19_amd64 amd64

# Download and extract Ubuntu 18.04 (i386)
RUN ./install_distrib.sh http://archive.ubuntu.com/ubuntu/dists/bionic-updates/main/installer-i386/current/images/netboot/netboot.tar.gz "Ubuntu 18.04 (i386) Installer" ubuntu-installer ubuntu-18_i386 i386

# Download and extract Ubuntu 18.04 (i386)
RUN ./install_distrib.sh http://ftp.nl.debian.org/debian/dists/buster/main/installer-i386/20190702+deb10u1/images/netboot/netboot.tar.gz "Debian Buster (i386) Installer" debian-installer debian-buster_i386 i386

# Install nfs-server
RUN apk add --no-cache --update nfs-utils openrc \
  && rc-update add nfsmount \
  && echo "tftpboot    /var/lib/tftpboot tftpboot      defaults        0       0" >> /etc/fstab \
  && echo "/var/lib/tftpboot "192.168.20.*"(ro,fsid=0,async,no_subtree_check,no_auth_nlm,insecure,no_root_squash)" >> /etc/fstab

# Mint and others
COPY tftpboot/ /var/lib/tftpboot
